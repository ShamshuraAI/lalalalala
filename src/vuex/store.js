import Vuex from 'vuex'
import actions from './actions/actions'
import getters from './getters/getters'
import mutations from './mutations/mutations'

let store = new Vuex.Store({
    state:{
        products:[],
        cart:[]
    },
    mutations,
    actions,
    getters
});

export default store;